TEAM=xsimic05_xjanos12_xmihul00
RM=rm -rf
INSTALL=install -D
SYMLINK=ln -sf
INSTALL_DIR=install -d
ZIP=zip -r
TAR=tar -cvzf
CP=cp

PACK_FILES=$(TEAM)/git.tar.gz $(TEAM)/doc.tar.gz $(TEAM)/Uživatelská\ příručka.pdf

ICONDIR=$(DESTDIR)/usr/share/icons/hicolor
DESKTOPDIR=$(DESTDIR)/usr/share/applications
APPDIR=$(DESTDIR)/usr/share/jmscalc
BINDIR=$(DESTDIR)/usr/bin

.PHONY: first all jmscalc clean pack doc check deb-pkg install uninstall

first: jmscalc

gui/Makefile:
	cd gui; qmake

math/libmath.a:
	$(MAKE) -C ./math

parser/libparser.a:
	$(MAKE) -C ./parser

jmscalc: gui/Makefile math/libmath.a parser/libparser.a
	$(MAKE) -C ./gui

clean: gui/Makefile
	$(MAKE) -C ./math clean
	$(MAKE) -C ./parser clean
	$(MAKE) -C ./gui clean
	$(RM) jmscalc "$(TEAM)/git.tar.gz" "$(TEAM)/doc.tar.gz"
	$(RM) "$(TEAM)/Uživatelská příručka.pdf" "$(TEAM)/"*.deb
	$(RM) $(TEAM).zip

check:
	$(MAKE) -C ./math check
	$(MAKE) -C ./parser check

$(TEAM):
	mkdir $(TEAM)

$(TEAM)/git.tar.gz: .git | $(TEAM)
	$(TAR) $@ .git

$(TEAM)/doc.tar.gz: doc | $(TEAM)
	$(TAR) $@ doc

$(TEAM)/Uživatelská\ příručka.pdf:
	$(CP) "documents/user guide.pdf" "$@"

pack: $(PACK_FILES) | $(TEAM)
	$(CP) -f ../jmscalc*.deb $(TEAM)
	$(ZIP) $(TEAM).zip $(TEAM)

doc:
	doxygen Doxyfile

deb-pkg:
	dpkg-buildpackage -b -rfakeroot

install: jmscalc
	$(INSTALL) "icons/hicolor/32x32/apps/jmscalc.png" "$(ICONDIR)/32x32/apps/jmscalc.png"
	$(INSTALL) "icons/hicolor/16x16/apps/jmscalc.png" "$(ICONDIR)/16x16/apps/jmscalc.png"
	$(INSTALL) "icons/hicolor/128x128/apps/jmscalc.png" "$(ICONDIR)/128x128/apps/jmscalc.png"
	$(INSTALL) "icons/hicolor/24x24/apps/jmscalc.png" "$(ICONDIR)/24x24/apps/jmscalc.png"
	$(INSTALL) "icons/hicolor/64x64/apps/jmscalc.png" "$(ICONDIR)/64x64/apps/jmscalc.png"
	$(INSTALL) "icons/hicolor/48x48/apps/jmscalc.png" "$(ICONDIR)/48x48/apps/jmscalc.png"
	$(INSTALL) "icons/hicolor/256x256/apps/jmscalc.png" "$(ICONDIR)/256x256/apps/jmscalc.png"
	$(INSTALL) "jmscalc" "$(APPDIR)/jmscalc"
	$(INSTALL) "jmscalc.desktop" "$(DESKTOPDIR)/jmscalc.desktop"
	$(INSTALL_DIR)   "$(BINDIR)"
	$(SYMLINK) "$(APPDIR)/jmscalc" "$(BINDIR)/jmscalc"

uninstall:
	$(RM) "$(ICONDIR)/32x32/apps/jmscalc.png"
	$(RM) "$(ICONDIR)/16x16/apps/jmscalc.png"
	$(RM) "$(ICONDIR)/128x128/apps/jmscalc.png"
	$(RM) "$(ICONDIR)/24x24/apps/jmscalc.png"
	$(RM) "$(ICONDIR)/64x64/apps/jmscalc.png"
	$(RM) "$(ICONDIR)/48x48/apps/jmscalc.png"
	$(RM) "$(ICONDIR)/256x256/apps/jmscalc.png"
	$(RM) "$(APPDIR)/jmscalc"
	$(RM) "$(DESKTOPDIR)/jmscalc.desktop"
	$(RM) "$(BINDIR)/jmscalc"