IVS - 3. Projekt
================

Informace
---------

### Stanovené úkoly a termíny

Úkol                                                    | Termín        |Řešitel
--------------------------------------------------------|---------------|---------
Matematická knihovna                                    | 7.4.2014      | xsimic05
Parsování výrazu                                        | 7.4.2014      | xjanos12
Vytvoření GUI                                           | 7.4.2014      | xmihul00
Testy matematické knihovny, programová dokumentace      | 14.4.2014     | xsimic05
Snímek aplikace, uživatelská příručka                   | 14.4.2014     | xmihul00
Makefile + instalátor                                   | 14.4.2014     | xjanos12

### Komunikační kanály

 - Osobní schůzky
 - Hangout
 - Email
 - Issue tracker


### Git Repozitář

[https://bitbucket.org/JOndra91/ivs](https://bitbucket.org/JOndra91/ivs)


Jak na vývoj
------------

Byla vytvořena jednoduchá adresářová struktura, pomocí které lze předejít složitému mergování.
Stěžejní části projektu budou řešeny ve vlastních adresářích.

 - Pro tvorbu uživatelského rozhraní slouží adresář `gui`
 - K matematické knihovně přísluší adresář `math`
 - Pro parsování matematických výrazů poslouží adresář `parser`

Každou z těchto částí je navíc vhodné řešit ve vlastních větvích.

### Větev feature-*

Pro různé vychytávky, které vyžadují větší úpravy je vhodné vytvořit vždy vlastní větev s názvem `feature-<název úpravy>`.
Po dokončení úpravy je možné slouči změny do větve `develop`.

### Větev develop

Větev `develop` slouží k úpravám funkčního kódu před sloučením s větví `master`.
Před sloučením změn do větve `master` by měl být zkompilovatelný, ideálně i bez chyb.