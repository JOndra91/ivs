/**
 * @file lexer.h
 * @author Ondřej Janošík <xjanos12@stud.fit.vutbr.cz>
 */

#ifndef LEXER_H
#define	LEXER_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <ctype.h>

#define LEXER_OK 1
#define LEXER_ERR 0

    typedef enum {
        L_PLUS = 1,
        L_MINUS,
        L_MULTIPLY,
        L_DIVIDE,
        L_POWER,
        L_FACTORIAL,
        L_BRACKET_LEFT,
        L_BRACKET_RIGHT,
        L_NUMBER,
        L_WORD,
        L_END
    } lexem;

    typedef struct {
        char* start;
        char* position;
        char* pool;
        unsigned long poolSize;
        unsigned long poolOffset;
        char decimalSeparator;
    } lexer;

    typedef struct {
        lexem type;
        char* param;
    } token;

    /**
     * Inicializuje strukturu lexeru pro zpracování výrazu
     * @param l lexer
     * @param expression výraz pro zpracování
     */
    void lexerPrepare(lexer *l, char* expression);

    /**
     * Uvolní paměť použitou lexerem
     * @param l lexer
     */
    void lexerFree(lexer *l);

    /**
     * Z výrazu spojeného s danou strukturou lexeru analyzuje následující token
     * @param l lexer
     * @param t token
     * @return 0 pokud došlo k úspěšné analýze tokenu
     */
    int lexerGetToken(lexer *l, token* t);

#ifdef	__cplusplus
}
#endif

#endif	/* LEXER_H */

