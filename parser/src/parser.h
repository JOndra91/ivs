/**
 * @file parser.h
 * @author Ondřej Janošík <xjanos12@stud.fit.vutbr.cz>
 */

#ifndef PARSER_H
#define	PARSER_H

#include "lexer.h"
#include <mathLibrary.h>
//#include "../math/mathLibrary.h"

#ifdef	__cplusplus
extern "C" {
#endif

#define PARSE_OK 1
#define PARSE_ERR 0

    typedef enum {
        S_PLUS,
        S_MINUS,
        S_MULTIPLY,
        S_DIVIDE,
        S_POWER,
        S_FACTORIAL,
        S_VALUE,
        S_BRACKET_LEFT,
        S_BRACKET_RIGHT,
        S_FUNCTION,
        S_END,
        S_TERM_COUNT,
        S_EXPR
    } symbol;

    typedef struct struct_expr {
        struct struct_expr *operands[2];
        int haveResult;
        mathtype result;
        mathcall eval;
    } expr;

    typedef struct {
        symbol s;
        token t;
        expr *e;
    } exprStackItem;

    typedef struct {
        exprStackItem *items;
        int size;
        int minSize;
        int top;
    } exprStack;

    /**
     * Analyzuje matematický výraz
     * @param expressionString Řetězec s matematickým výrazem
     * @param decimalSeparator Oddělovač desetinných míst
     * @param e Struktura s vyhodnotitelným matematickým výrazem
     * @return PARSE_OK - při úspěchu, jinak PARSE_ERR
     */
    int parserExecute(char *expressionString, char decimalSeparator, expr **e);

    /**
     * Vyhodnotí matematický výraz
     * @param e Struktura s vyhodnotitelným matematickým výrazem
     * @param error Kód chyby, pokud vše proběhlo v pořádku, tak je hodnota MATH_NO_ERR
     * @return Při úspěchu vrací hodnotu výrazu. Pokud je nastavená chybová zpráva, výsledek může být nedefinovaný.
     */
    mathtype expressionResult(expr *e, mathErr *error);

    /**
     * Vyhradí paměť pro strukturu s výrazem a provede inicializaci
     * @return Ukazatel na výraz
     */
    expr* exprAlloc();

    /**
     * Uvolní paměť vyhrazenou pro strukturu s výrazem
     * @param e Struktura s výrazem
     * @param recursive Zda uvolnění paměti proběhne přes celou stromovou strukturu výrazů
     */
    void exprFree(expr *e, int recursive);

    /**
     * Inicializuje zásobník pro parser
     * @param stack Zásobníková struktura
     * @param size Výchozí velikost zásobníku
     */
    void stackInit(exprStack *stack, int size);

    /**
     * Uvolní paměť vyhrazenou pro zásobník
     * @param stack Zásobníková struktura
     */
    void stackFree(exprStack *stack);

    /**
     * Odebere prvek z vrcholu zásobníku
     * @param stack Zásobníková struktura
     */
    void stackPop(exprStack *stack);

    /**
     * Vloží prvek na vrchol zásobníku
     * @param stack Zásobníková struktura
     * @param s Symbol pro vložení na zásobník
     * @param t Token pro vložení na zásobník nebo NULL
     * @param e Výraz pro vložení na zásobník
     */
    void stackPush(exprStack *stack, symbol s, token *t, expr *e);

    /**
     * Vrátí prvek na vrcholu zásobníku
     * @param stack Zásobníková struktura
     * @param s Symbol na vrcholu zásobníku
     * @param t Token na vrcholu zásobníku nebo NULL
     * @param e Výraz na vrcholu zásobníku
     * @return 1 pokud zásobník není prázdný, jinak 0
     */
    int stackTop(exprStack *stack, symbol *s, token *t, expr **e);

    /**
     * Vrátí terminál nejblíže k vrcholu zásobníku
     * @param stack Zásobníková struktura
     * @param s Symbol na vrcholu zásobníku
     * @param t Token na vrcholu zásobníku nebo NULL
     * @param e Výraz na vrcholu zásobníku
     * @return 1 pokud je v zásobníku terminál, jinak 0
     */
    int stackTerm(exprStack *stack, symbol *s, token *t, expr **e);

    /**
     * Vratí, zda je zásobník prázdný
     * @param stack Zásobníková struktura
     * @return 1 pokud je zásobník prázdný, jinak 0
     */
    int stackEmpty(exprStack *stack);

#ifdef	__cplusplus
}
#endif

#endif	/* PARSER_H */

