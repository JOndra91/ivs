/**
 * @file parser.c
 * @author Ondřej Janošík <xjanos12@stud.fit.vutbr.cz>
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "parser.h"


#define IS_TERM(s) (s != S_EXPR)
#define IS_EXPR(s) (s == S_EXPR)
#define H 128
#define E 127
#define L 126
#define I 0

int precTable[S_TERM_COUNT][S_TERM_COUNT] = {
	//        +  -  *  /  ^  !  V  (  )  F  $
	/* + */ { H, H, L, L, L, L, L, L, H, L, H},
	/* - */ { H, H, L, L, L, L, L, L, H, L, H},
	/* * */ { H, H, H, H, L, L, L, L, H, L, H},
	/* / */ { H, H, H, H, L, L, L, L, H, L, H},
	/* ^ */ { H, H, H, H, H, H, L, L, H, H, H},
	/* ! */ { H, H, H, H, H, H, L, L, H, H, H},
	/* V */ { H, H, H, H, H, H, I, I, H, H, H},
	/* ( */ { L, L, L, L, L, L, L, L, E, H, H},
	/* ) */ { L, L, L, L, L, L, I, H, H, H, I},
	/* F */ { I, I, I, I, I, I, I, L, I, I, I},
	/* $ */ { L, L, L, L, L, L, L, L, I, L, I},
};

symbol lexemToSymbol[] = {
	[L_PLUS] = S_PLUS,
	[L_MINUS] = S_MINUS,
	[L_MULTIPLY] = S_MULTIPLY,
	[L_DIVIDE] = S_DIVIDE,
	[L_POWER] = S_POWER,
	[L_FACTORIAL] = S_FACTORIAL,
	[L_BRACKET_LEFT] = S_BRACKET_LEFT,
	[L_BRACKET_RIGHT] = S_BRACKET_RIGHT,
	[L_NUMBER] = S_VALUE,
	[L_WORD] = S_FUNCTION,
	[L_END] = S_END
};

mathcall operators[] = {
	[S_PLUS] = addition,
	[S_MINUS] = subtraction,
	[S_MULTIPLY] = multiplication,
	[S_DIVIDE] = division,
	[S_POWER] = power
};

/**
 * Podle názvu funkce vrátí ukazatel na funkci
 * @param name Název funkce
 * @return Ukazatel na funkci nebo NULL
 */
inline mathcall getFunction(char *name) {
	if (strcmp(name, "sqrt") == 0) {
		return squareRoot;
	}

	return NULL;
}

/**
 * Převede token na symbol
 * @param t Token
 * @return Identifikátor symbolu nebo -1
 */
inline symbol tokenToSymbol(token *t) {
	if (t->type <= L_END) {
		return lexemToSymbol[t->type];
	}

	return -1;
}

/**
 * Redukuje výraz na zásobníku
 * @param stack Struktura zásobníku
 * @return PARSE_OK při úspěchu, jinak PARSE_ERR
 */
inline int parserReduce(exprStack *stack) {
	symbol sym0, sym1;
	token tok0, tok1;
	expr *e0, *e1;

	stackTop(stack, &sym0, &tok0, &e0);
	stackPop(stack);

	if (sym0 == S_VALUE) {
		e0 = exprAlloc();
		e0->haveResult = 1;
		sscanf(tok0.param, "%lf", &(e0->result));

		stackPush(stack, S_EXPR, NULL, e0);

	}
	else if (sym0 == S_FACTORIAL) {
		e0 = exprAlloc();
		e0->eval = factorial;

		stackTop(stack, &sym0, &tok0, &e1);
		stackPop(stack);

		e0->operands[0] = e1;

		if (sym0 != S_EXPR) {
			return PARSE_ERR;
		}

		stackPush(stack, S_EXPR, NULL, e0);

	}
	else if (sym0 == S_EXPR) {
		stackTop(stack, &sym1, &tok1, &e1);
		stackPop(stack);

		if (sym1 == S_BRACKET_LEFT) {

			stackTop(stack, &sym1, &tok1, &e1);
			if (sym1 == S_FUNCTION) {
				stackPop(stack);
				e1 = exprAlloc();
				e1->eval = getFunction(tok1.param);
				e1->operands[0] = e0;

				if (e1->eval == NULL) {
					return PARSE_ERR;
				}

				e0 = e1;
			}

			stackPush(stack, S_EXPR, NULL, e0);
		}
		else if (sym1 >= S_PLUS && sym1 <= S_POWER) {
			e1 = exprAlloc();
			e1->eval = operators[sym1];
			e1->operands[1] = e0;

			stackTop(stack, &sym0, NULL, &e0);

			switch (sym1) {
				case S_PLUS:
				case S_MINUS:

					if (sym0 != S_EXPR) {
						e0 = exprAlloc();
						e0->haveResult = 1;
						e0->result = 0;
						break;
					}

				default:
					stackPop(stack);
					e1->operands[0] = e0;
			}

			stackPush(stack, S_EXPR, NULL, e1);
		}
		else {
			return PARSE_ERR;
		}

	}
	else {
		return PARSE_ERR;
	}

	return PARSE_OK;

}

int parserExecute(char* expressionString, char decimalSeparator, expr** e) {
	int p;
	lexer l;
	token tokIn, tokSt;
	symbol symIn, symSt;
	exprStack stack;
	expr *e0;

	lexerPrepare(&l, expressionString);
	l.decimalSeparator = decimalSeparator;
	stackInit(&stack, 128);
	stackPush(&stack, S_END, NULL, NULL);

	if (lexerGetToken(&l, &tokIn)) {
		symIn = tokenToSymbol(&tokIn);

		while (symIn != -1 && IS_TERM(symIn)) {

			stackTerm(&stack, &symSt, &tokSt, &e0);

			if (symSt == S_END && symIn == S_END) {
				stackTop(&stack, &symSt, &tokSt, e);
				stackPop(&stack); // Should be last non-terminal (S_EXPR)
				stackPop(&stack); // Should be last terminal (S_END)

				if (!stackEmpty(&stack)) {
					break;
				}

				stackFree(&stack);
				lexerFree(&l);
				return PARSE_OK;
			}

			p = precTable[symSt][symIn];

			if (p == I) {
				break;
			}
			else if (p == H) {
				if (!parserReduce(&stack)) {
					break;
				}
			}
			else if (p == E) {
				if (!parserReduce(&stack)) {
					break;
				}
				lexerGetToken(&l, &tokIn);
				symIn = tokenToSymbol(&tokIn);
			}
			else {
				stackPush(&stack, symIn, &tokIn, NULL);
				lexerGetToken(&l, &tokIn);
				symIn = tokenToSymbol(&tokIn);
			}
		}
	}

	// Uvolnění paměti po chybě
	while (stackTop(&stack, &symSt, &tokSt, &e0)) {
		if (symSt == S_EXPR) {
			exprFree(e0, 1);
		}

		stackPop(&stack);
	}

	lexerFree(&l);
	stackFree(&stack);
	return PARSE_ERR;
}

expr* exprAlloc() {
	expr *e;
	e = (expr*) malloc(sizeof (expr));

	e->eval = NULL;
	e->haveResult = 0;
	e->operands[0] = NULL;
	e->operands[1] = NULL;
	e->result = 0.0;

	return e;
}

void exprFree(expr *e, int recursive) {

	if (e != NULL) {

		if (recursive) {
			if (e->operands[0] != NULL) exprFree(e->operands[0], 1);
			if (e->operands[1] != NULL) exprFree(e->operands[1], 1);
		}

		free(e);
	}
}

mathtype expressionResult(expr* e, mathErr *error) {
	int i;
	mathtype operands[2];

	*error = MATH_NO_ERR;

	if (!e->haveResult) {
		for (i = 0; i < 2; i++) {
			if (e->operands[i] != NULL) {
				operands[i] = expressionResult(e->operands[i], error);

				if (*error != MATH_NO_ERR) {
					return (mathtype) 0.0;
				}
			}
		}

		e->result = e->eval(operands, error);
		e->haveResult = 1;
	}

	return e->result;
};

void stackInit(exprStack* stack, int size) {
	stack->minSize = size;
	stack->size = size;
	stack->top = -1;

	stack->items = (exprStackItem*) (malloc(sizeof (exprStackItem) * size));
}

void stackFree(exprStack* stack) {
	free(stack->items);
	stack->items = NULL;
	stack->top = -1;
	stack->size = 0;
	stack->minSize = 0;
}

int stackEmpty(exprStack* stack) {
	return stack->top == -1;
}

void stackPush(exprStack* stack, symbol s, token* t, expr *e) {
	exprStackItem *i;

	if (++stack->top == stack->size) {
		stack->size <<= 1;
		stack->items = (exprStackItem*) realloc(stack->items, sizeof (exprStackItem) * stack->size);
	}

	i = stack->items + stack->top;

	i->s = s;
	i->e = e;
	if (t != NULL) {
		i->t = *t;
	}
}

void stackPop(exprStack* stack) {
	if (!stackEmpty(stack)) {
		stack->top--;
	}
}

int stackTop(exprStack* stack, symbol* s, token* t, expr **e) {
	exprStackItem *i;

	if (stackEmpty(stack)) {
		return 0;
	}

	i = stack->items + stack->top;

	*s = i->s;
	*e = i->e;
	if (t != NULL) {
		*t = i->t;
	}

	return 1;
}

int stackTerm(exprStack* stack, symbol* s, token* t, expr **e) {
	exprStackItem *i;

	if (stackEmpty(stack)) {
		return 0;
	}

	i = stack->items + stack->top;

	while (!IS_TERM(i->s)) {
		--i;
		if (i < stack->items) {
			return 0;
		}
	}

	*s = i->s;
	*e = i->e;
	if (t != NULL) {
		*t = i->t;
	}

	return 1;
}