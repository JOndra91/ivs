/**
 * @file lexer.c
 * @author Ondřej Janošík <xjanos12@stud.fit.vutbr.cz>
 */

#include <stdlib.h>
#include <string.h>

#include "lexer.h"

#define GET_CH (*(t->param + length))
#define NEXT_CH {do{length++;} while(isblank(GET_CH));}
#define POOL_SIZE 4096

#define isdecimal(c) (c == l->decimalSeparator)

static int lexTable[255] = {
	['\0'] = L_END,
	['+'] = L_PLUS,
	['-'] = L_MINUS,
	['*'] = L_MULTIPLY,
	['/'] = L_DIVIDE,
	['!'] = L_FACTORIAL,
	['^'] = L_POWER,
	['('] = L_BRACKET_LEFT,
	[')'] = L_BRACKET_RIGHT
};

void lexerPrepare(lexer *l, char* expression) {
	l->start = expression;
	l->position = expression;
	l->poolOffset = 0;
	l->poolSize = POOL_SIZE;
	l->pool = (char*) malloc(sizeof (char) * POOL_SIZE);
	l->decimalSeparator = '.';
};

void lexerFree(lexer* l) {
	free(l->pool);
	l->pool = NULL;
}

int lexerGetToken(lexer* l, token* t) {

	char *p;
	int length;

	t->param = l->position;
	length = 0;

	// TODO: Desetinný oddělovač

	if (isdigit(GET_CH) || isdecimal(GET_CH)) {
		t->type = L_NUMBER;

		if (!isdecimal(GET_CH)) {
			NEXT_CH;
			while (isdigit(GET_CH)) {
				NEXT_CH;
			}
		}

		if (isdecimal(GET_CH)) {
			NEXT_CH;

			while (isdigit(GET_CH)) {
				NEXT_CH;
			}
		}

		if (GET_CH == 'e' || GET_CH == 'E') {
			NEXT_CH;

			if (GET_CH == '+' || GET_CH == '-') {
				NEXT_CH;
			}

			if (isdigit(GET_CH)) {
				NEXT_CH;

				while (isdigit(GET_CH)) {
					NEXT_CH;
				}
			}
			else {
				return LEXER_ERR;
			}
		}
	}
	else if (isalpha(GET_CH)) {
		t->type = L_WORD;

		NEXT_CH;
		while (isalpha(GET_CH)) {
			NEXT_CH;
		}
	}
	else if (lexTable[(int) GET_CH] != 0) {
		t->type = lexTable[(int) GET_CH];
		NEXT_CH;
	}
	else {
		return LEXER_ERR;
	}

	l->position += length;

	if (l->poolOffset + length >= l->poolSize) {
		l->poolSize += POOL_SIZE;
		l->pool = (char*) realloc(l->pool, l->poolSize);
	}

	p = l->pool + l->poolOffset;
	strncpy(p, t->param, length);
	l->poolOffset += length + 1; // String terminator
	p[length] = '\0';
	t->param = p;

	return LEXER_OK;
};
