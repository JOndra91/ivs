#include <stdio.h>

#include "../src/parser.h"

int ret;

void testExpressionResult(char *expression, mathtype expectedResult) {
	expr *e;
	mathtype res;
	mathErr err;

	if (parserExecute(expression, '.', &e)) {
		res = expressionResult(e, &err);


		if (res == expectedResult) {
			printf("Expression:\n\t%s\nOK: Result: %f\tExpected: %f\n\n", expression, res, expectedResult);
		}
		else {
			printf("Expression:\n\t%s\nERR: Result: %f\tExpected: %f\n\n", expression, res, expectedResult);
			ret = 1;
		}

		exprFree(e, 1);
	}
	else {
		ret = 1;
		printf("Expression:\n\t%s\nERR: Syntax error\n\n", expression);
	}
}

void testExpressionFail(char *expression) {
	expr *e;

	if (parserExecute(expression, '.', &e)) {
		ret = 1;
		printf("Expression:\n\t%s\nERR: No error\n\n", expression);
	}
	else {
		printf("Expression:\n\t%s\nOK: Syntax error\n\n", expression);
	}
}

int main(int argc, char** argv) {

	ret = 0;

	testExpressionResult("4+6", 10);
	testExpressionResult("4-6", -2);
	testExpressionResult("2*2", 4);
	testExpressionResult("100/10", 10);
	testExpressionResult("100/32", 3.125);
	testExpressionResult("1+2*3", 7);
	testExpressionResult("(1+2)*3", 9);
	testExpressionResult("(2+3)", 5);
	testExpressionResult("2^32 -   1", 4294967295);
	testExpressionResult("sqrt(4)", 2);
	testExpressionResult("4!", 1 * 2 * 3 * 4);
	testExpressionResult("3!!", 720);
	testExpressionResult("(3!!-700)^2", 400);
	testExpressionResult("314", 314);
	testExpressionResult("3.14", 3.14);
	testExpressionResult(".14", .14);
	testExpressionResult("-.14", -.14);
	testExpressionResult("-.14e3", -.14e3);
	testExpressionResult("2e3", 2e3);
	testExpressionResult("32.2", 32.2);
	testExpressionResult("5000e-2", 5000e-2);
	testExpressionResult("-7", -7);
	testExpressionFail("sqrt 4");
	testExpressionFail("(6)(4)");

	return ret;
}
