/**
 * @file:   mathLibrary.h
 * @author: Tomáš Šimíček <xsimic05@stud.fit.vutbr.cz>
 */

#ifndef MATHLIBRARY_H
#define	MATHLIBRARY_H

#ifdef	__cplusplus
extern "C" {
#endif

    typedef enum {
        MATH_NO_ERR, // Žádná chyba
        MATH_DIV_ZERO, // Dělení nulou
        MATH_POWER_NOT_INTEGER, // Mocnina není celé číslo
        MATH_FACT_NOT_INTEGER, // Faktoriál není celé číslo
        MATH_NEGATIVE_ROOT // Záporná odmocnina
    } mathErr;

    typedef double mathtype;
    typedef mathtype(*mathcall)(mathtype*, mathErr*);

#define MATHCALL(name) mathtype name(mathtype* param, mathErr *error)

    MATHCALL(addition);
    MATHCALL(subtraction);
    MATHCALL(multiplication);
    MATHCALL(division);
    MATHCALL(power);
    MATHCALL(factorial);
    MATHCALL(squareRoot);

#ifdef	__cplusplus
}
#endif

#endif	/* MATHLIBRARY_H */

