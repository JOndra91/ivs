/**
 * @file:   tests.c
 * @author: Tomáš Šimíček <xsimic05@stud.fit.vutbr.cz>
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mathLibrary.h"

char *messages[] = {
	[MATH_NO_ERR] = "MATH_NO_ERR",
	[MATH_DIV_ZERO] = "MATH_DIV_ZERO",
	[MATH_POWER_NOT_INTEGER] = "MATH_POWER_NOT_INTEGER",
	[MATH_FACT_NOT_INTEGER] = "MATH_FACT_NOT_INTEGER",
	[MATH_NEGATIVE_ROOT] = "MATH_NEGATIVE_ROOT"
};

/**
 * Testy pro matematickou knihovnu
 */
int main() {
	mathtype a[2], res;
	mathErr err;

	//hodnoty 1-2 test
	a[0] = 3;
	a[1] = 4;

	//Test1 - addision
	res = addition(a, &err);

	if (res == (mathtype) 7.0) {
		printf("test1: OK\n");
	} else {
		printf("test1: ERROR, count = %f\n", res);
	}

	//Test2 - subtraction
	res = subtraction(a, &err);
	if (res == (mathtype) - 1.0) {
		printf("test2: OK\n");
	} else {
		printf("test2: ERROR, count = %f\n", res);
	}

	//hodnoty 3-4 test
	a[0] = 10;
	a[1] = 2;

	//Test3 - multiplication
	res = multiplication(a, &err);
	if (res == (mathtype) 20.0) {
		printf("test3: OK\n");
	} else {
		printf("test3: ERROR, count = %f\n", res);
	}

	//Test4 - multiplication
	res = division(a, &err);
	if (res == (mathtype) 5.0) {
		printf("test4: OK\n");
	} else {
		printf("test4: ERROR, count = %f\n", res);
	}

	//hodnoty 5-6 test
	a[0] = 5;
	a[1] = 2;

	//Test5 - multiplication
	res = multiplication(a, &err);
	if (res == (mathtype) 10.0) {
		printf("test5: OK\n");
	} else {
		printf("test5: ERROR, count = %f\n", res);
	}

	//Test6 - multiplication
	res = division(a, &err);
	if (res == (mathtype) 2.5) {
		printf("test6: OK\n");
	} else {
		printf("test6: ERROR, count = %f\n", res);
	}

	//hodnoty 7-8 test
	a[0] = 10;
	a[1] = 0;

	//Test7 - multiplication
	res = multiplication(a, &err);
	if (res == (mathtype) 0.0) {
		printf("test7: OK\n");
	} else {
		printf("test7: ERROR, count = %f\n", res);
	}

	//Test8 - division
	res = division(a, &err);
	if (err == MATH_DIV_ZERO) {
		printf("test8: OK - %s\n", messages[MATH_DIV_ZERO]);
	} else {
		printf("test8: ERROR - expected %s, got %s\n", messages[MATH_DIV_ZERO], messages[err]);
	}

	//hodnoty  9 test
	a[0] = 2;
	a[1] = 4;

	//Test9 - power
	res = power(a, &err);
	if (res == (mathtype) 16.0) {
		printf("test9: OK\n");
	} else {
		printf("test9: ERROR, count = %f\n", res);
	}

	//hodnoty  10 test
	a[0] = 2;
	a[1] = 0;

	//Test10 - multiplication
	res = power(a, &err);
	if (res == (mathtype) 1.0) {
		printf("test10: OK\n");
	} else {
		printf("test10: ERROR, count = %f\n", res);
	}


	//hodnoty  11 test
	a[0] = 2;
	a[1] = -2;

	//Test11 - multiplication
	res = power(a, &err);
	if (res == (mathtype) 0.25) {
		printf("test11: OK\n");
	} else {
		printf("test11: ERROR, count = %f\n", res);
	}

	//hodnoty  12 test
	a[0] = 2;
	a[1] = -3;

	//Test12 - multiplication
	res = power(a, &err);
	if (res == (mathtype) 0.125) {
		printf("test12: OK\n");
	} else {
		printf("test12: ERROR, count = %f\n", res);
	}

	//hodnoty  13 test
	a[0] = 2;
	a[1] = 2.5;

	//Test13 - power
	res = power(a, &err);
	if (err == MATH_POWER_NOT_INTEGER) {
		printf("test13: OK - %s\n", messages[MATH_POWER_NOT_INTEGER]);
	} else {
		printf("test13: ERROR - expected %s, got %s\n", messages[MATH_POWER_NOT_INTEGER], messages[err]);
	}

	//hodnoty  14 test
	a[0] = 2;
	a[1] = 2.999;

	//Test14 - power
	res = power(a, &err);
	if (err == MATH_POWER_NOT_INTEGER) {
		printf("test14: OK - %s\n", messages[MATH_POWER_NOT_INTEGER]);
	} else {
		printf("test14: ERROR - expected %s, got %s\n", messages[MATH_POWER_NOT_INTEGER], messages[err]);
	}

	//hodnoty  15 test
	a[0] = 0;

	//Test15 - power
	res = factorial(a, &err);
	if (res == (mathtype) 1) {
		printf("test15: OK\n");
	} else {
		printf("test15: ERROR, count = %f\n", res);
	}


	//hodnoty  16 test
	a[0] = 1;

	//Test14 - power
	res = factorial(a, &err);
	if (res == (mathtype) 1) {
		printf("test16: OK\n");
	} else {
		printf("test16: ERROR, count = %f\n", res);
	}

	//hodnoty  17 test
	a[0] = 5;

	//Test17 - factorial
	res = factorial(a, &err);
	if (res == (mathtype) 120) {
		printf("test17: OK\n");
	} else {
		printf("test17: ERROR, count = %f\n", res);
	}

	//hodnoty  18 test
	a[0] = 5.2;

	//Test18 - factorial
	res = factorial(a, &err);
	if (err == MATH_FACT_NOT_INTEGER) {
		printf("test18: OK - %s\n", messages[MATH_FACT_NOT_INTEGER]);
	} else {
		printf("test18: ERROR - expected %s, got %s\n", messages[MATH_FACT_NOT_INTEGER], messages[err]);
	}

	//hodnoty  19 test
	a[0] = 25;

	//Test19 - square
	res = squareRoot(a, &err);
	if (res == (mathtype) 5) {
		printf("test19: OK\n");
	} else {
		printf("test19: ERROR, count = %f\n", res);
	}

	//hodnoty  20 test
	a[0] = -25;

	//Test20 - squareRoot
	res = squareRoot(a, &err);
	if (err == MATH_NEGATIVE_ROOT) {
		printf("test20: OK - %s\n", messages[MATH_NEGATIVE_ROOT]);
	} else {
		printf("test20: ERROR - expected %s, got %s\n", messages[MATH_NEGATIVE_ROOT], messages[err]);
	}

	return (EXIT_SUCCESS);
}

