/**
 * @file:   mathLibrary.c
 * @author: Tomáš Šimíček <xsimic05@stud.fit.vutbr.cz>
 */

#include <stdio.h>

#include "mathLibrary.h"

mathtype getPower(mathtype a, long n);

/**
 * Sčítání
 * @param param pole
 * @param error chyba
 * @return výsledek
 */
MATHCALL(addition) {
	*error = MATH_NO_ERR;
	return (mathtype) (param[0] + param[1]);
}

/**
 * Odčítáná
 * @param param pole
 * @param error chyba
 * @return výsledek
 */
MATHCALL(subtraction) {
	*error = MATH_NO_ERR;
	return (mathtype) (param[0] - param[1]);
}

/**
 * Násobení
 * @param param pole
 * @param error chyba
 * @return výsledek
 */
MATHCALL(multiplication) {
	*error = MATH_NO_ERR;
	return (mathtype) (param[0] * param[1]);
}

/**
 * Dělení
 * @param param pole
 * @param error chyba
 * @return výsledek
 */
MATHCALL(division) {
	*error = MATH_NO_ERR;
	if ((mathtype) param[1] == 0.0) {
		*error = MATH_DIV_ZERO;
		return (mathtype) 0.0;
	}
	return (mathtype) (param[0] / param[1]);
}

/**
 * Umocňování
 * @param param pole
 * @param error chyba
 * @return výsledek
 */
MATHCALL(power) {
	*error = 0;
	long exponent = (long) param[1];

	if (param[1] - exponent != 0) {
		*error = MATH_POWER_NOT_INTEGER;
		return (mathtype) 0.0;
	}

	if (exponent == 0) {
		return (mathtype) 1;
	}
	else if (exponent < 0) {
		return (mathtype) (1 / getPower(param[0], -exponent));
	}
	else {
		return getPower(param[0], exponent);
	}
}

/**
 * Faktoriál
 * @param param pole
 * @param error chyba
 * @return výsledek
 */
MATHCALL(factorial) {
	mathtype res = 1.0;
	long base = (long) param[0], f;

	*error = 0;

	if (param[0] - base != 0) {
		*error = MATH_FACT_NOT_INTEGER;
		return (mathtype) 0.0;
	}

	for (f = 1; f <= base; f++) {
		res *= f;
	}

	return (mathtype) res;
}

/**
 * Odmocňování
 * @param param pole
 * @param error chyba
 * @return výsledek
 */
MATHCALL(squareRoot) {
	*error = 0;
	mathtype m = (mathtype) param[0];

	if (m < (mathtype) 0.0) {
		*error = MATH_NEGATIVE_ROOT;
		return (mathtype) 0.0;
	}

	int i = 0;
	while ((i * i) <= m) {
		i++;
	}
	i--;

	mathtype d = m - i*i;
	mathtype p = d / (2 * i);
	mathtype a = i + p;
	return (mathtype) (a - (p * p) / (2 * a));
}

/**
 * Pomocná funkce pro výpočet faktoriálu
 * @param a základ
 * @param n n-tý faktoriál
 * @return výsledek
 */
mathtype getPower(mathtype a, long n) {
	int i;
	mathtype res = a;

	for (i = 1; i < n; i++) {
		res *= a;
	}

	return (mathtype) res;
}
