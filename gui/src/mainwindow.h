#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QKeyEvent>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    /*
    void on_pushButton_7_clicked();

    void on_pushButton_8_clicked();

    void on_pushButton_9_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_5_clicked();

    void on_pushButton_6_clicked();

    void on_pushButton_1_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_0_clicked();

    void on_pushButton_dot_clicked();

    void on_pushButton_left_clicked();

    void on_pushButton_right_clicked();

    void on_pushButton_dividing_clicked();

    void on_pushButton_on2_clicked();

    void on_pushButton_multiplication_clicked();

    void on_pushButton_sqrt_clicked();

    void on_pushButton_minus_clicked();

    void on_pushButton_inverted_clicked();

    void on_pushButton_plus_clicked();

    void on_pushButton_C_clicked();

    void on_pushButton_DEL_clicked();
    */

    void on_pushButton_equals_clicked();

    void on_lineEdit_returnPressed();

    void on_lineEdit_keyPressed(QKeyEvent *event);

    void Undo();

    void Redo();

private:
    Ui::MainWindow *ui;
    QList<QString> examples;
    int exampleIndex;

    void type_to_line(QString text);
    void type_to_line(QString text, int move);
    void compute(QString example);
};

#endif // MAINWINDOW_H
