#include "lineeditkeypress.h"

LineEditKeyPress::LineEditKeyPress(QWidget *parent) :
    QLineEdit(parent)
{
}

void LineEditKeyPress::keyPressEvent(QKeyEvent *event)
{
    emit keyPress(event);
    QLineEdit::keyPressEvent(event);
}
