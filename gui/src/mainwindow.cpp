#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QLocale>
#include <parser.h>
#include <mathLibrary.h>

QString errMessages[] = {
	[MATH_NO_ERR] = "",
	[MATH_DIV_ZERO] = "Division by zero",
	[MATH_POWER_NOT_INTEGER] = "Exponent is not an integer",
	[MATH_FACT_NOT_INTEGER] = "Factorial is not an integer",
	[MATH_NEGATIVE_ROOT] = "Root of negative number"
};

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    exampleIndex(0)
{
    ui->setupUi(this);

    this->centralWidget()->setLayout(ui->verticalLayout);
    ui->label->setTextInteractionFlags(Qt::TextSelectableByMouse);
    ui->label_2->setTextInteractionFlags(Qt::TextSelectableByMouse);

    connect(ui->lineEdit, SIGNAL(keyPress(QKeyEvent*)), this, SLOT(on_lineEdit_keyPressed(QKeyEvent*)));
    connect(ui->actionUndo, SIGNAL(triggered()), this, SLOT(Undo()));
    connect(ui->actionRedo, SIGNAL(triggered()), this, SLOT(Redo()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::type_to_line(QString text)
{
    type_to_line(text, 1);
}

void MainWindow::type_to_line(QString text, int move)
{
    QString t = ui->lineEdit->text();
    int i = ui->lineEdit->cursorPosition();
    t.insert(i, text);
    ui->lineEdit->setText(t);
    ui->lineEdit->setCursorPosition(i+move);
    ui->lineEdit->setFocus();
}

void MainWindow::compute(QString expression)
{
    if(expression.isEmpty())
        return;

    expr *e;
    mathtype result;
    mathErr err;

    QCursor c = this->cursor();
    this->setCursor(Qt::WaitCursor);

    ui->label->setText(expression);

    expression.replace('.', QLocale::system().decimalPoint());
    expression.replace(',', QLocale::system().decimalPoint());

    if (parserExecute(const_cast<char*> (expression.toStdString().c_str()), QLocale::system().decimalPoint().toLatin1(), &e)) {
        result = expressionResult(e, &err);

        if(err != MATH_NO_ERR) {
            ui->label_2->setText("Math error: " + errMessages[err]);
        } else {
            ui->label_2->setText(QString::number(result));
        }

        exprFree(e, 1);
    }
    else {
        ui->label_2->setText("Syntax error!");
    }

    this->examples.append(expression);
    this->exampleIndex = this->examples.count();

    ui->lineEdit->setText("");
    ui->lineEdit->setFocus();
    this->setCursor(c);
}

void MainWindow::Undo()
{
    if(this->examples.count() > 0)
    {
        if(this->exampleIndex > 0)
        {
            this->exampleIndex--;
        }
        ui->lineEdit->setText(this->examples.at(this->exampleIndex));
    }
}

void MainWindow::Redo()
{
    if(this->examples.count() > 0)
    {
        this->exampleIndex++;
        if(this->exampleIndex < this->examples.count())
        {
            ui->lineEdit->setText(this->examples.at(this->exampleIndex));
        }
        else if(this->exampleIndex >= this->examples.count())
        {
            ui->lineEdit->setText("");
            this->exampleIndex = this->examples.count();
        }
    }
}

void MainWindow::on_lineEdit_keyPressed(QKeyEvent *event)
{
    if(event->key() == Qt::Key_Up)
    {
        Undo();
    }
    else if(event->key() == Qt::Key_Down)
    {
        Redo();
    }
}
/*
void MainWindow::on_pushButton_7_clicked()
{
    type_to_line("7");
}

void MainWindow::on_pushButton_8_clicked()
{
    type_to_line("8");
}

void MainWindow::on_pushButton_9_clicked()
{
    type_to_line("9");
}

void MainWindow::on_pushButton_4_clicked()
{
    type_to_line("4");
}

void MainWindow::on_pushButton_5_clicked()
{
    type_to_line("5");
}

void MainWindow::on_pushButton_6_clicked()
{
    type_to_line("6");
}

void MainWindow::on_pushButton_1_clicked()
{
    type_to_line("1");
}

void MainWindow::on_pushButton_2_clicked()
{
    type_to_line("2");
}

void MainWindow::on_pushButton_3_clicked()
{
    type_to_line("3");
}

void MainWindow::on_pushButton_0_clicked()
{
    type_to_line("0");
}

void MainWindow::on_pushButton_dot_clicked()
{
    type_to_line(".");
}

void MainWindow::on_pushButton_left_clicked()
{
    type_to_line("(");
}

void MainWindow::on_pushButton_right_clicked()
{
    type_to_line(")");
}

void MainWindow::on_pushButton_dividing_clicked()
{
    type_to_line("/");
}

void MainWindow::on_pushButton_on2_clicked()
{
    type_to_line("");
}

void MainWindow::on_pushButton_multiplication_clicked()
{
    type_to_line("*");
}

void MainWindow::on_pushButton_sqrt_clicked()
{
    type_to_line("");
}

void MainWindow::on_pushButton_minus_clicked()
{
    type_to_line("-");
}

void MainWindow::on_pushButton_inverted_clicked()
{
    type_to_line("");
}

void MainWindow::on_pushButton_plus_clicked()
{
    type_to_line("+");
}


void MainWindow::on_pushButton_C_clicked()
{
    ui->lineEdit->setText("");
}

void MainWindow::on_pushButton_DEL_clicked()
{
    QString t = ui->lineEdit->text();
    int i = ui->lineEdit->cursorPosition();
    t.remove(i-1, 1);
    ui->lineEdit->setText(t);
    ui->lineEdit->setCursorPosition(i-1);
    ui->lineEdit->setFocus();
}
*/

void MainWindow::on_pushButton_equals_clicked()
{
    compute(ui->lineEdit->text());
}

void MainWindow::on_lineEdit_returnPressed()
{
    compute(ui->lineEdit->text());
}
