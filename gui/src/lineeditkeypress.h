#ifndef LINEEDITKEYPRESS_H
#define LINEEDITKEYPRESS_H

#include <QLineEdit>
#include <QKeyEvent>
#include <QWidget>

class LineEditKeyPress : public QLineEdit
{
    Q_OBJECT
public:
    explicit LineEditKeyPress(QWidget *parent = 0);

signals:
    void keyPress(QKeyEvent *event);

public slots:
    void keyPressEvent(QKeyEvent *event);
};

#endif // LINEEDITKEYPRESS_H
