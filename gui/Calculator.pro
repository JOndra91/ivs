#-------------------------------------------------
#
# Project created by QtCreator 2014-04-07T18:24:36
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = jmscalc
TEMPLATE = app

DESTDIR = ../
OBJECTS_DIR = build/
MOC_DIR = moc/
UI_DIR = ui/

SOURCES += src/main.cpp\
        src/mainwindow.cpp \
        src/lineeditkeypress.cpp

HEADERS  += src/mainwindow.h \
            src/lineeditkeypress.h

FORMS    += forms/mainwindow.ui

INCLUDEPATH += ../math ../parser/src
LIBS += -L../parser/ -lparser -L../math/ -lmath

RESOURCES += \
    src/resource.qrc
